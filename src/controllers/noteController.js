/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../utils/apiUtils');
const {
  addUserNotes,
  getUserNoteById,
  deleteUserNoteById,
  getUserNotes,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
} = require('../services/noteServices');

router.post(
    '/',
    asyncWrapper(async (req, res) => {
      const {_id} = req.user;

      await addUserNotes(_id, req.body);

      res.json({message: 'Note was created succesfully!'});
    }),
);


router.get(
    '/',
    asyncWrapper(async (req, res) => {
      const {_id} = req.user;
      const {limit, offset} = req.body;

      const listOfNotes = await getUserNotes(_id, limit, offset);

      res.status(200).json(listOfNotes);
    }),
);


router.get(
    '/:id',
    asyncWrapper(async (req, res) => {
      const noteId = req.params.id;

      const note = await getUserNoteById(noteId);

      res.status(200).json({note});
    }),
);

router.put(
    '/:id',
    asyncWrapper(async (req, res) => {
      const noteId = req.params.id;
      const data = req.body;

      await updateUserNoteById(noteId, data);

      res.status(200).json({message: 'Note was updated succesfully'});
    }),
);


router.patch(
    '/:id',
    asyncWrapper(async (req, res) => {
      const noteId = req.params.id;

      await toggleCompletedForUserNoteById(noteId);

      res.status(200).json({message: 'Success'});
    }),
);


router.delete(
    '/:id',
    asyncWrapper(async (req, res) => {
      const {id} = req.params;
      const {_id} = req.user;

      await deleteUserNoteById(id, _id);

      res.status(200).json({message: 'Note was deleted succesfully'});
    }),
);


module.exports = {
  notesRouter: router,
};
