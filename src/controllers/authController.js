/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {login, createProfile} = require('../services/authUser');
// const {
//   registrationValidator,
// } = require('../middlewares/validationMiddleware');
const {asyncWrapper} = require('../utils/apiUtils');

router.post(
    '/register',
    // registrationValidator,
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      await createProfile({username, password});
      res.json({message: `Account was created succesfully!`});
    }),
);

router.post(
    '/login',
    asyncWrapper(async (req, res) => {
      const {username, password} = req.body;

      const token = await login({username, password});

      res.json({message: `Logged in succesfully!`, jwt_token: token});
    }),
);

module.exports = {
  authRouter: router,
};
