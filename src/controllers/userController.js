/* eslint-disable new-cap */
const express = require('express');
const router = express.Router();

const {asyncWrapper} = require('../utils/apiUtils');

const {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
} = require('../services/userServices');


router.get('/me', asyncWrapper(async (req, res) => {
  console.log(req);
  const {_id} = req.user;

  const user = await getProfileInfo(_id);
  res.status(200).json({user});
}));


router.delete('/me', asyncWrapper(async (req, res) => {
  const {_id} = req.user;

  await deleteProfile(_id);
  res.status(200).json({message: 'Profile deleted successfully'});
}));

router.patch('/me',

    asyncWrapper(async (req, res) => {
      const {_id} = req.user;
      const {oldPassword, newPassword} = req.body;

      await changeProfilePassword(_id, oldPassword, newPassword);

      res.status(200).json({message: 'Success'});
    }),
);


module.exports = {
  usersRouter: router,
};
