// const {notesRouter} = require('../controllers/noteController');
const {Note} = require('../models/noteModel');

const addUserNotes = async (userId, notePayLoad) => {
  const note = new Note({...notePayLoad, userId});

  await note.save();
};

const deleteUserNoteById = async (noteId, userId) => {
  await Note.findOneAndRemove({_id: noteId, userId});
};

const getNotesByUserId = async (userId) => {
  const notes = await Note.find({userId});
  return notes;
};

const getUserNotes = async (_id, limit = 2, offset = 0) => {
  const listOfNotes =
  await Note
      .find({userId: _id}, {__v: 0})
      .skip(parseInt(offset)).limit(parseInt(limit));
  return {
    offset,
    limit,
    count: listOfNotes.length,
    notes: listOfNotes,
  };
};


const getUserNoteById = async (noteId) => {
  return await Note.findOne({_id: noteId}, {__v: 0});
};


const updateUserNoteById = async (noteId, data) => {
  await Note.findOneAndUpdate(
      {_id: noteId},
      {$set: data},
  );
};

const toggleCompletedForUserNoteById = async (noteId) => {
  const note = await Note.findOne({_id: noteId});

  const noteStatus = note.completed;
  if (noteStatus) {
    note.completed = false;
  } else {
    note.completed = true;
  }

  await note.save();
};


module.exports = {
  addUserNotes,
  deleteUserNoteById,
  getNotesByUserId,
  getUserNotes,
  getUserNoteById,
  updateUserNoteById,
  toggleCompletedForUserNoteById,
};
