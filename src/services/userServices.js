const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');


const getProfileInfo = async (_id) => {
  return await User.findOne({_id}, {__v: 0});
};

const deleteProfile = async (_id) => {
  await User.remove({_id});
};

const changeProfilePassword = async (userId, oldPsw, newPsw) => {
  const user = await User.findOne({_id: userId});

  if (!(await bcrypt.compare(oldPsw, user.password))) {
    throw new Error('Wrong password data');
  }
  await User.findOneAndUpdate(
      {_id: userId},
      {$set: {password: await bcrypt.hash(newPsw, 10)}},
  );
};

module.exports = {
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};
