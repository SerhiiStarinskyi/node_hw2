const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

require('dotenv').config({path: '../src/config/.env'});
const SECRET = process.env.SECRET;

const {User} = require('../models/userModel');

const createProfile = async ({username, password}) => {
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  await user.save();
};

const login = async ({username, password}) => {
  const user = await User.findOne({username});

  if (!user) {
    throw new Error('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new Error('Invalid email or password');
  }

  const token = jwt.sign(
      {
        _id: user._id,
        username: user.username,
      },
      SECRET,
  );

  return token;
};

module.exports = {
  login,
  createProfile,
};
