const asyncWrapper = (clb) => {
  return (req, res, next) => clb(req, res).catch(next);
};

module.exports = {
  asyncWrapper,
};
