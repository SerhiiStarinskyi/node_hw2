const jwt = require('jsonwebtoken');

require('dotenv').config({path: '../src/config/.env'});
const SECRET = process.env.SECRET;

const authMiddleware = (req, res, next) => {
  const {authorization} = req.headers;

  if (!authorization) {
    return res
        .status(401)
        .json({message: 'Please, provide "authorization" header'});
  }

  // Bearer - тип токена, token
  const [, token] = authorization.split(' ');

  if (!token) {
    return res
        .status(401)
        .json({message: 'Please, include token to request'});
  }

  try {
    // tokenPayLoad - user data
    const tokenPayLoad = jwt.verify(token, SECRET);
    req.user = {
      _id: tokenPayLoad._id,
      username: tokenPayLoad.username,
    };
    next();
  } catch (err) {
    res.status(401).json({message: err.message});
  }
};

module.exports = {
  authMiddleware,
};
