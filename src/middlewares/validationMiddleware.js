const Joi = require('joi');

const registrationValidator = async (req, res, next) => {
  const schema = Joi.object({
    username: Joi.string().alphanum().min(3).max(10).optional(),
    password: Joi.string()
        .min(8)
        .max(20)
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),
    email: Joi.string()
        .email({
          minDomainSegments: 2,
          tlds: {allow: ['com', 'net']},
        })
        .required(),
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    next(err);
  }
};

module.exports = {
  registrationValidator,
};
